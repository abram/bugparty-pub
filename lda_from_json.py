import json
import lda
import os
import sys
# http://localhost:5984/bugparty/_all_docs?include_docs=true
# file = open("everything.json")
import scipy.spatial.distance
ntopics = 20

def dumptojsonfile( filename, data):
    file = open(filename, "w")
    file.write(json.dumps(data, indent=2))
    file.close()

#jsonsdata = open("small.json").read()
jsonsdata = open("large.json").read()
#jsonsdata = sys.stdin.read()
#sys.stdin.close()
data = json.loads(jsonsdata)
ldocs = [ row["doc"] for row in data["rows"] ]
docs = {}



for doc in ldocs:
    docs[doc["_id"]] = doc

ids = [ doc["_id"] for doc in ldocs ]

ldocs_by_id = docs

docs, dicts = lda.load_lda_docs(docs, ids)
dumptojsonfile("out/dicts.txt", dicts)
filename, _ = lda.make_vr_lda_input( docs, dicts )
command = lda.vm_lda_command(filename, ntopics)
print(command)
os.system( command )

topics, summary = lda.summarize_topics_from_file( ntopics, dicts, ("out/topics-%s.dat" % ntopics) )
document_topic_matrix = lda.summarize_document_topic_matrix_from_file( ntopics, ("out/predictions-%s.dat" % ntopics) )
doc_top_mat_map = dict( (ids[i], document_topic_matrix[i]) for i in range(0,len(ids)) )
dumptojsonfile("out/lda-topics.txt", topics)
dumptojsonfile("out/summary.txt", summary)
dumptojsonfile("out/document_topic_matrix.txt", document_topic_matrix)
dumptojsonfile("out/document_topic_map.txt", doc_top_mat_map)
# fuck what if I have to do the transpose?
x = document_topic_matrix
#x = transpose( document_topic_matrix )
#y = scipy.spatial.distance.pdist( x, 'cosine' )


#cosines = lda.compact_cosine( x, ids )
#print("COSINES")
#dumptojsonfile("out/document_distance.txt", cosines)

out = nn( x, ids )

# import pyflann
# pyflann.set_distance_type('kl')
# flann = FLANN()
# result, dists = flann.nn(array(x),array(x),10)#,algorithm='kmeans')
# [ldocs[i]["title"] for i in result[341]]
# 
# build_params.target_precision = 0.9;
# build_params.build_weight = 0.01;
# build_params.memory_weight = 0;
# 
# [index, parameters] = pyflann.flann.build_index(x);#, build_params);
# result = flann_search(index,testset,5,parameters);
# flann_free_index(index);
# 

    #y2 = scipy.spatial.distance.squareform(y)
# import pylab
# pylab.imshow(y2)
# pylab.show()
