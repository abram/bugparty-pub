#
#> > > Yes, it's possible to further train a previously fit model. If your
#> > > data are in two files called articles1.dat and articles2.dat, and
#> > > there are 100K articles in each dataset, and a vocabulary size between
#> > > 8192 and 16383, you could do something like
#> > > ./vw articles1.dat --lda 100 -b 14 --minibatch 512 --lda_D 200000 -f
#model.dat
#> > > to train using the first 100K documents and then
#> > > ./vw articles2.dat --lda 100 -b 14 --minibatch 512 --lda_D 200000 -i
#> > > model.dat -f model2.dat --initial_t 100000
#> > > to train using the next 100K documents. You want --lda_D to be as
#> > > close as possible to the final number of documents you're going to
#> > > use, although it probably doesn't matter too much once it gets large.
#> > > --initial_t should be some constant (e.g., the default value of 0)
#> > > plus the number of iterations you've done so far (e.g., 0 initially,
#> > > 100000 after looking at 100000 documents, etc.).
#> > >
#> > > As was pointed out a couple of emails ago on the list, you can do
#> > > inference on new documents without further training using something
#> > > like
#> > > ./vw articles3.dat --lda 20 -i model2.dat --testonly -p predictions.dat
#> > >
#> > > Hope that helps!

VW=/home/hindle1/src/vowpal_wabbit/vowpalwabbit/vw
$VW --lda 20  --minibatch 1  --initial_t 1 -b 15 -p predictionsaaa1.dat --readable_model topicsaaa1.dat -f topicsaaa1.model 0-999
$VW --lda 20  --minibatch 1  --initial_t 1 -b 15 -p predictionsaaa2.dat --readable_model topicsaaa2.dat -i topicsaaa1.model -f topicsaaa2.model 1000-1999
$VW --lda 20  --minibatch 1  --initial_t 1 -b 15 -p predictionsaaa3.dat --readable_model topicsaaa3.dat -i topicsaaa2.model -f topicsaaa3.model 2000-2999

