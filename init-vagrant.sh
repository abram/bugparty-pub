#!/bin/sh
# The purpose of this script is to setup vagrant on a box run by a csuser
# essentially we can't use NFS so we need to use /local/scratch
USER=`id -un`
mkdir /local/scratch/$USER
mkdir /local/scratch/$USER/.vagrant.d
ln -s  /local/scratch/$USER/.vagrant.d $HOME/.vagrant.d
mkdir /local/scratch/$USER/vagrant
ln -s  /local/scratch/$USER/vagrant $HOME/vagrant
mkdir /local/scratch/$USER/.VirtualBox
ln -s  /local/scratch/$USER/.VirtualBox $HOME/.VirtualBox
mkdir /local/scratch/$USER/VirtualBox\ VMs
ln -s  /local/scratch/$USER/VirtualBox\ VMs $HOME/VirtualBox\ VMs
export PATH=$PATH:/opt/vagrant/bin
cd ~/vagrant
mkdir precise32
cd precise32
#  	http://files.vagrantup.com/precise32.box
vagrant box add precise32 /local/scratch/precise32.box
vagrant init precise32
vagrant up
vagrant halt
